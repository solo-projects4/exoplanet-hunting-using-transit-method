from tkinter import *
import tkinter as tk
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
matplotlib.use("TkAgg")

root = tk.Tk()
root.title('Exoplanet Hunting Using Transit Method')
root.iconbitmap('logo.ico')
root.geometry("850x600")
root.resizable(0, 0)

# grid formatting

# empty label that status text will replace after the button is pressed
# done explicitly for formatting purposes
status_label = Label(root, text="        ")
status_label.grid(row=2, column=1)

empty_figure = Figure(figsize=(6.5, 5), dpi=100)
plot = empty_figure.add_subplot(111)

canvas = FigureCanvasTkAgg(empty_figure, master=root)
canvas.draw()

get_widz = canvas.get_tk_widget()
get_widz.grid(row=3, column=1, rowspan=50, columnspan=1, pady=(0, 0), padx=10)




