import tkinter as tk
import pandas as pd
import matplotlib
import tkinter.messagebox
from input_star import *
from plot_graph import *
from input_suggestion import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
matplotlib.use("TkAgg")


def statusButtonClick():
    inputValue = textbox.get("1.0", END) 
    exoplanet_status = Label(root, text=" ")

    if 0 < int(inputValue) <= 38:
        exoplanet_status.config(text="     This star is confirmed to have at least one exoplanet in orbit      ")
    elif 38 < int(inputValue) <= 5088:
        exoplanet_status.config(text="This star is currently not considered to have any exoplanets in orbit")
    else:
        pass

    exoplanet_status.grid(row=2, column=1)


def clicked():
    inputValue = textbox.get("1.0", END)
    true_input_value = inputValue.strip()

    try:
        # temporary variable val checking if input is integer
        val = int(true_input_value)
        # unable to use try-catch block for range because of different matrix dimension
        # plotting matrix values that don't exist causes error much earlier
        if 1 > val or val > 5088:
            tkinter.messagebox.showerror("Error", "Numbers need to be from the [1, 5088] range")
            cleanGraph()
    except ValueError:
        tkinter.messagebox.showerror("Error", "Please only input whole numbers")
        cleanGraph()


# if there is an error, e.g. input exceeds interval, previous graph will be erased
def cleanGraph():
    empty_graph = Figure(figsize=(6.5, 5), dpi=100)
    empty_graph.add_subplot(111)
    clean_canvas = FigureCanvasTkAgg(empty_graph, master=root)
    clean_canvas.draw()
    get_widget = clean_canvas.get_tk_widget()
    get_widget.grid(row=3, column=1, rowspan=50, columnspan=1)


Show_graph_button = Button(root, text="Show graph", bd='5',
                           command=lambda: [clicked(), showGraph(textbox.get("1.0", END))])
Status_Button = Button(root, text="  Status  ", bd='5', command=lambda: [clicked(), statusButtonClick()])

Status_Button.grid(row=1, column=4, pady=(10, 0), padx=(5, 0))
Show_graph_button.grid(row=1, column=5, pady=(10, 0), padx=(0, 0))

