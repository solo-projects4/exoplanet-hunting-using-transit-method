import tkinter as tk
import tkinter.font as tkFont
from tkinter import *
from GUI import *

# define font
fontStyle = tkFont.Font(family="Lucida Grande", size=12)

textbox = tk.Text(font=fontStyle, height=1, width=50, borderwidth=2, fg="grey")
textbox.insert(tk.END, "Input star number [1, 5088]")
textbox.grid(row=1, column=1, pady=(10, 0))


# setting disappearing default textbox message
def default(event):
    current = textbox.get("1.0", tk.END)
    if current == "Input star number [1, 5088]\n":
        textbox.delete("1.0", tk.END)
    elif current == "\n":
        textbox.insert("1.0", "Input star number [1, 5088]")


textbox.bind("<FocusIn>", default)
textbox.bind("<FocusOut>", default)

