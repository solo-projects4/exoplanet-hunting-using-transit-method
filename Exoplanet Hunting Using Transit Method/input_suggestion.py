from tkinter import *
import tkinter as tk
import pandas as pd
from tkinter.font import BOLD
from input_star import *
from plot_graph import *


suggestionLabel = Label(root, text=' Good transit examples: ', font=('Arial', 9, BOLD))
suggestionLabel.grid(row=3, column=4, columnspan=5,pady=(10, 0), padx=0, sticky=N+W)

var = IntVar()
R1 = Radiobutton(root, text="Number 1", variable=var, value=1)
R1.grid(row=4, column=4, columnspan=2, sticky=N+W)
R2 = Radiobutton(root, text="Number 4", variable=var, value=4)
R2.grid(row=5, column=4, columnspan=2, sticky=N+W)
R3 = Radiobutton(root, text="Number 5", variable=var, value=5)
R3.grid(row=6, column=4, columnspan=2, sticky=N+W)
R4 = Radiobutton(root, text="Number 14", variable=var, value=14)
R4.grid(row=7, column=4, columnspan=2, sticky=N+W)
R5 = Radiobutton(root, text="Number 15", variable=var, value=15)
R5.grid(row=8, column=4, columnspan=2, sticky=N+W)
R6 = Radiobutton(root, text="Number 23", variable=var, value=23)
R6.grid(row=9, column=4, columnspan=2, sticky=N+W)
R7 = Radiobutton(root, text="Number 26", variable=var, value=26)
R7.grid(row=10, column=4, columnspan=2, sticky=N+W)

SuggestionButton = Button(root, text="Load Graph", bd=5, command=lambda: showGraph(str(var.get())))
SuggestionButton.grid(row=11, column=4, columnspan=2, sticky=N+W)
DefaultLabel = Label(root, text='Default: 1 ')
DefaultLabel.grid(row=12, column=4, columnspan=2, sticky=N+W)

