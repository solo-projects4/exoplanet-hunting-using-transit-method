from tkinter import *
import tkinter as tk
import pandas as pd
from tkinter.font import BOLD
from input_star import *


def plotGraph(x_axis, y_axis):
    fig = Figure(figsize=(6.5, 5), dpi=100)
    # plotting inside figure
    # 1 row, 1 col, index 1
    graph = fig.add_subplot(111)
    graph.plot(x_axis, y_axis, color='blue', linestyle='solid', marker='|')
    graph.set_xlabel("Time In Days")
    graph.set_ylabel("Relative Brightness")
    graph.set_title("Light Curve")
    graph.grid()

    graph_canvas = FigureCanvasTkAgg(fig, master=root)
    graph_canvas.draw()
    get_widget = graph_canvas.get_tk_widget()
    get_widget.grid(row=3, column=1, rowspan=50, columnspan=1)


def showGraph(inputValue):
    # flux.1 - flux.3197 => column names in csv file
    cols = []
    for i in range(1, 3198):
        cols.append('FLUX.' + str(i))

    # observations were made over approx. 80 days
    time_series = pd.read_csv("time_sample_interval.csv")

    data = pd.read_csv("exoTrain.csv", usecols=cols,  skiprows=range(1, int(inputValue) - 1), nrows=1)
    x_axis = time_series.values
    y_axis = data.values
    plotGraph(x_axis, y_axis)
