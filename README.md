# Exoplanet Hunting Using Transit Method

This project helps user visualize how exoplanet hunting using transit method works.

    Transit method is a photometric method that aims to
    indirectly detect the presence of exoplanets in orbit
    around a star. The transit is occurred when an 
    exoplanet passes in front of its star. If luminosity 
    of a star is periodically decreased by approximately 
    the same value, we can say it is a good candidate for 
    future research and use of different methods to find 
    out if they really have an exoplanet/s in its orbit. 

Program uses public available data from Kepler telescope observation of 5088 deep space stars that happened over the course of
approx. 80 days. You can find the data here --> https://www.kaggle.com/datasets/keplersmachines/kepler-labelled-time-series-data






It provides light curves of all the stars observed and easy access to light curves of exoplanet candidate stars.
You can just input star number and the graph will be plotted. First 38 stars are either confirmed or are good candidates for
having orbiting planets around them. If you want to see one of those stars graph, just input one number from 1 to 38.
Best examples are given on the right side of the program's main window.

You can see the examples of the program shown in the pictures attached to the project.
Enjoy!
